FROM gorialis/discord.py:3.9.4-buster-master-full

WORKDIR /app  
COPY requirements.txt ./
RUN ["pip", "install", "--no-cache-dir", "-r", "requirements.txt"]

COPY ./bot/ .
CMD ["python", "bot.py"]
